import React, { Component } from 'react';
import _ from 'lodash';
import {Button,Modal} from 'react-bootstrap';
import './style.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import HorizontalForm from '../HorizontalForm';
import {AIQX_ROLE_CONSTANTS,INITIAL_PANEL_STATE,AIQX_ADMIN_SERVER_REST_ENDPOINT} from '../../modules/constants' ;
import PanelHeader from '../PanelHeader';
import axios from 'axios';


export default class FormsPanel extends Component {
  constructor() {
      super();
      this.state = INITIAL_PANEL_STATE;
      
      this.handleAddMoreUser = this.handleAddMoreUser.bind(this);
      this.handleRemoveUser = this.handleRemoveUser.bind(this);
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmitAll = this.handleSubmitAll.bind(this);
      this.hideModal = this.hideModal.bind(this);
      this.showModal = this.showModal.bind(this);
      
    }

     shouldComponentUpdate(nextProps, nextState) {
        return !_.isEqual(this.state.users, nextState.users) ||
               !_.isEqual(this.state.showModal, nextState.showAlert);
    }

  handleAddMoreUser = () => {
    this.setState({
        users: this.state.users.concat([
          {userName:'',
          password:'',
          advertiserID:'',
          role:AIQX_ROLE_CONSTANTS['client'],
          idx:'' }])
      });
    }

  handleRemoveUser = (idx) => {
    this.setState({
      users: this.state.users.filter((user,_idx) => _idx !== idx)
    });
  };

  handleChange = (user) => {
    let users = this.state.users.map((x,_idx) => {
      if(_idx !== user.idx)return x;
      return user;
    });
    this.setState({users:users});
  };

  handleSubmitAll = () => {
    if(this.state.users.length === 0 || this.state === INITIAL_PANEL_STATE) return ;
    console.log('entries',this.state.users);
    this.showModal();
    
  }

  hideModal() {
    this.setState({ showModal: false });
  }

  showModal() {
    this.setState({ showModal: true });
  }

  handleSendToServer = () => {
    this.setState({ showModal: false });
     axios.post(AIQX_ADMIN_SERVER_REST_ENDPOINT,{ 
            users:this.state.users
          })
         .then((res) => {
            console.log(res);
            this.setState(INITIAL_PANEL_STATE);
         });
    
  }

  render() {  
    return (
      <div className = "forms-main-panel">
          <Modal show={this.state.showModal} onHide={this.hideModal}>
            <Modal.Header>
                <Modal.Title>Confirmation
                </Modal.Title>
            </Modal.Header>
            
            <Modal.Body>
                <p> The entered {this.state.users.length} {this.state.users.length >1 ? "entries":"entry"} will be sent to LDAP. Are you sure you want to proceed ? 
                </p>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={this.hideModal}>Cancel</Button>
                <Button onClick={this.handleSendToServer}>Yes</Button>

            </Modal.Footer>
          </Modal>
        <PanelHeader className = "header"/>
        <hr/>
        <div className = "user-forms-group">
          { this.state.users.map((user, idx) => (
              <div className="input-fields-row">
                  <HorizontalForm user = {{...user,idx:idx}}
                                handleChange = {this.handleChange}
                                handleRemoveUser = {this.handleRemoveUser} 
                  />
              </div>))
          }
          <div className = "add-new-row">
            <Button bsStyle = "success" onClick = {()=>this.handleAddMoreUser()}><strong>+</strong></Button>
          </div>
        </div>
        <div className = "create-button-container">
          <Button bsStyle = "primary" onClick = {() => this.handleSubmitAll()}>CREATE</Button>
        </div>
      </div>
    );
  }
}

