import React, { Component } from 'react';
import _ from 'lodash';
import {Button,FormGroup,ControlLabelo,FormControl,ControlLabel,Form} from 'react-bootstrap';
import './style.css';
import {AIQX_ROLE_CONSTANTS} from '../../modules/constants';

export default class HorizontalForm extends React.Component {
  constructor() {
    super();
    this.handleChange = this.handleChange.bind(this);    
  }

  shouldComponentUpdate(nextProps, nextState) {    
        return !_.isEqual(nextProps.user, this.props.user)
               || !_.isEqual(nextProps.handleChange, this.props.handleChange)
               || !_.isEqual(nextProps.handleRemoveUser, this.props.handleRemoveUser);
               
    }

    handleChange = (evt) => {
      let {user} = this.props;
      let fieldChanged = evt.target.placeholder != null ? 
                evt.target.placeholder : "role";
      user = {
                ...user,
                [fieldChanged]:evt.target.value
             }
      this.props.handleChange(user);
    }


  render() {  
    return (
      <Form inline className = "inline-form">
        { 
          Object.keys(this.props.user).map((field) => 
            field !== "idx" &&  field!=="role" ?
            (
              <FormControl type = {field === "userName" ? "email" :"text"} 
                     placeholder={field} 
                     value ={this.props.user[field]}
                     onChange = {(e)=>this.handleChange(e)}
                     className = "input-field"
              />
            ):""
            )  
        }
        <FormControl componentClass = "select" 
                     placeholder="role" 
                     onChange = {(e)=> this.handleChange(e)} 
                     className = "input-field" 
                     >
          <option value = {AIQX_ROLE_CONSTANTS["client"]}> {AIQX_ROLE_CONSTANTS["client"]} </option>
          <option value = {AIQX_ROLE_CONSTANTS["admin"]}> {AIQX_ROLE_CONSTANTS["admin"]}</option>
        </FormControl>

        <Button  bsStyle="danger" 
                  onClick= {()=>this.props.handleRemoveUser(this.props.user.idx)}
                  className = "input-field" 
                  >
                  <strong><small>x</small></strong>
        </Button>

      </Form>
    )
  }
}
