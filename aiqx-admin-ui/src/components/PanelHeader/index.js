import React, { Component } from 'react';
import './style.css';

export default class PanelHeader extends React.Component {
  
    render(){
        return (
            <div className = "header">
                <img src = 'images/aiqx.png' alt='aiqx-logo'/>
                <span className = "admin-header-text"><strong>Admin</strong></span>
            </div>
        );
    }
  }