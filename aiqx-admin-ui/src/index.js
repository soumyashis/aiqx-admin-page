import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import FormsPanel from './components/FormsPanel';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<FormsPanel />, document.getElementById('root'));
