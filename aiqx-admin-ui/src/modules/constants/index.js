export const AIQX_ROLE_CONSTANTS = {
    admin : "AIQX_CLIENT_ADMIN",
    client : "AIQX_CLIENT",
}

export const INITIAL_PANEL_STATE = { 
    users:[
        { userName:'',
          password:'',
          advertiserID:'',
          role:AIQX_ROLE_CONSTANTS["client"],
          idx:''
        }],
    showModal:false
};

export const AIQX_ADMIN_SERVER_REST_ENDPOINT = "http://localhost:8080/ldap/addUsers";