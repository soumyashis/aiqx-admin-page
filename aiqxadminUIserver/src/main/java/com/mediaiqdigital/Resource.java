package com.mediaiqdigital;

import com.mediaiqdigital.util.createLdapEntryUtil;
import com.mediaiqdigital.util.model.User;
import com.mediaiqdigital.util.model.UserGroup;

import org.apache.directory.api.ldap.model.entry.*;
import org.apache.directory.api.ldap.model.exception.LdapEntryAlreadyExistsException;
import org.apache.directory.api.ldap.model.exception.LdapNoSuchObjectException;
import org.apache.directory.api.ldap.model.exception.LdapSchemaViolationException;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.apache.directory.ldap.client.api.LdapNetworkConnection;
import org.apache.directory.ldap.client.api.exception.InvalidConnectionException;
import com.mediaiqdigital.constants.aiqxAdminConstants.*;


import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import static com.mediaiqdigital.constants.aiqxAdminConstants.*;

@Path("/ldap")
public class Resource {

    private LdapConnection connection;

    @POST
    @Path("addUsers")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Entry addUsers(final UserGroup userGroup) {

        try {
            //connection = new LdapNetworkConnection("0.0.0.0", 10389); //trial purpose
            connection = new LdapNetworkConnection("23.21.238.167", 389);

            System.out.println("Connecting to ldap server...");

            connection.setTimeOut(0);  //let connection be active unless explicitly closed

            //connection.bind("uid=admin,ou=system", "secret");                 //trial purpose
            connection.bind("cn=admin,dc=mediaiqdigital,dc=com", "root");

            System.out.println("connected to server");

            for (final User user : userGroup.getUsers()) {
                System.out.println(user);

                if (user.getUserName() == "") continue;

                final Entry entryToAdd = createLdapEntryUtil.createNewLdapEntry(
                        user.getUserName(),
                        user.getPassword(),
                        user.getAdvertiserID());

                System.out.println(entryToAdd);

                if (!connection.exists(entryToAdd.getDn())) {
                    try {
                        connection.add(entryToAdd);
                        System.out.println("entry added successfully");
                    } catch (Exception e) {
                        System.out.print("failed to add entry ");
                        e.printStackTrace();
                        System.out.println(user.getUserName());

                    }

                    final Modification modificationAssignClientRole = new DefaultModification(
                            ModificationOperation.ADD_ATTRIBUTE,
                            UNIQUE_MEMBER_ATTRIBUTE,
                            entryToAdd.getDn().toString());

                    final Modification modificationAssignClientAdminRole = new DefaultModification(
                            ModificationOperation.ADD_ATTRIBUTE,
                            UNIQUE_MEMBER_ATTRIBUTE,
                            entryToAdd.getDn().toString());


                    try {
                        //if its a client entry, add only in AIQX_CLIENT else add in AIQX_CLIENT_ADMIN too
                        connection.modify(DN_OF_ENTRY_TO_ASSIGN_CLIENT_ROLE, modificationAssignClientRole);

                        if (user.getRole().equals(AIQX_CLIENT_ADMIN_ROLE)) {
                            connection.modify(DN_OF_ENTRY_TO_ASSIGN_CLIENT_ADMIN_ROLE, modificationAssignClientAdminRole);
                        }

                    } catch (Exception e) {
                        System.out.println("failed to assign role to entry");
                        e.printStackTrace();
                    }

                }

            }

            System.out.println("Closing connection...");
            connection.close();
            System.out.println("Connection closed");

            return null;
        } catch (InvalidConnectionException e) {
            System.out.println("invalid connection / connection refused by server");
            return new DefaultEntry();
        } catch (LdapSchemaViolationException e) {
            e.printStackTrace();
            System.out.println("Schema Violation Exception");
            return new DefaultEntry();

        } catch (LdapEntryAlreadyExistsException e) {
            e.printStackTrace();
            System.out.println("Entry already exists");
        } catch (LdapNoSuchObjectException e) {
            System.out.println("No such entry type exists");
            return new DefaultEntry();

        } catch (Exception e) {
            e.printStackTrace();
            return new DefaultEntry();

        }

        return null;
    }

}
