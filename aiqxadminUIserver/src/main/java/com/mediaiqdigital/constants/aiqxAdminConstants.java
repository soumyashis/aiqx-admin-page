package com.mediaiqdigital.constants;

public class aiqxAdminConstants {

    public static final String DN_OF_ENTRY_TO_ASSIGN_CLIENT_ROLE = "cn=AIQX_CLIENT,ou=groups,dc=mediaiqdigital,dc=com";
    public static final String DN_OF_ENTRY_TO_ASSIGN_CLIENT_ADMIN_ROLE = "cn=AIQX_CLIENT_ADMIN,ou=groups,dc=mediaiqdigital,dc=com";
    public static final String UNIQUE_MEMBER_ATTRIBUTE = "uniqueMember";
    public static final String AIQX_CLIENT_ADMIN_ROLE = "AIQX_CLIENT_ADMIN";

}
