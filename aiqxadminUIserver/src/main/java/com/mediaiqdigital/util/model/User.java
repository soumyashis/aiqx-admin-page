package com.mediaiqdigital.util.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;

public class User {
    @JsonProperty
    private String userName;
    @JsonProperty
    private String password;
    @JsonProperty
    private String advertiserID;
    @JsonProperty
    private String role;
    @JsonProperty
    private String idx;

    public String getUserName() {
        return userName;
    }

    public void setUserName(final String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public String getAdvertiserID() {
        return advertiserID;
    }

    public void setAdvertiserID(final String advertiserID) {
        this.advertiserID = advertiserID;
    }

    public String getRole() {
        return role;
    }

    public void setRole(final String role) {
        this.role = role;
    }

    public String getIdx() {
        return idx;
    }

    public void setIdx(final String idx) {
        this.idx = idx;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("userName", userName)
                .add("password", password)
                .add("advertiserID", advertiserID)
                .add("role", role)
                .add("idx", idx)
                .toString();
    }
}
