package com.mediaiqdigital.util;

import org.apache.directory.api.ldap.model.entry.DefaultEntry;
import org.apache.directory.api.ldap.model.entry.Entry;

public class createLdapEntryUtil {
    public static Entry createNewLdapEntry(final String cn, final String password, final String advertiserID) {

        try {
            final Entry newEntry;
            newEntry = new DefaultEntry(
                    "cn=" + cn + ",ou=users,dc=mediaiqdigital,dc=com"/*,
                    "ObjectClass:groupOfPermissions"*/,
                    "ObjectClass:inetOrgPerson",
                    "ObjectClass:organizationalPerson",
                    "ObjectClass:top",
                    "ObjectClass:person",
                    "ObjectClass:groupOfPermissions",
                    "userPassword:" + password,
                    "sn:" + advertiserID
                    // temp fix (no other field available for advertiser ID ,
                    // sn is always null anyways)
            );
            return newEntry;

        } catch (Exception e) {
            e.printStackTrace();
            return new DefaultEntry();
        }
    }

}
